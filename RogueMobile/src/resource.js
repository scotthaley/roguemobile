var res = {
    HelloWorld_png : "res/HelloWorld.png",
    CloseNormal_png : "res/CloseNormal.png",
    CloseSelected_png : "res/CloseSelected.png",
    Player_png : "res/placeholders/player/player_sheet.png",
    Player_plist : "res/placeholders/player/player.plist",
    Tiles_png : "res/placeholders/map/tile_sheet.png",
    Tiles_plist : "res/placeholders/map/map.plist"
};

//var rooms = {
//	room1 : "res/placeholders/map/rooms/room1.csv"
//};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
//for (var i in rooms) {
//	g_resources.push(rooms[i]);
//}