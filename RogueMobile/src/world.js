var GroundTile = {
	Top1 : "#tile-3-2.png",
	Top2 : "#tile-5-1.png",
	Top3 : "#tile-5-2.png",
	Top4 : "#tile-5-3.png",
	TopGrass : "#tile-3-1.png",
	WallBL : "#tile-0-0.png",
	WallBR : "#tile-0-3.png",
	WallB : "#tile-0-1.png",
	WallTR : "#tile-3-3.png",
	WallTL : "#tile-3-0.png",
	WallL : "#tile-4-0.png",
	WallR : "#tile-4-3.png",
	WallBack1 : "#tile-4-2.png",
	WallBack2 : "#tile-4-1.png"
};

var DecorationTile = {
	Grass1 : "#tile-2-1.png",
	Grass2 : "#tile-2-0.png",
	Grass3 : "#tile-2-3.png",
	Stalactite1 : "#tile-1-0.png"
}

var BackgroundTile = {
	Back1 : "#tile-1-1.png",
	Back2 : "#tile-1-2.png",
	Back3 : "#tile-1-3.png",	
}

var ObstacleTile = {
	LavaSolid : "#tile-1-4.png",
	LavaTop1 : "#tile-0-4.png",
	LavaTop2 : "#tile-0-5.png"
}

Object.prototype.hasOwnValue = function(val) {
	for(var prop in this) {
		if(this.hasOwnProperty(prop) && this[prop] === val) {
			return true;   
		}
	}
	return false;
};


var World = cc.Layer.extend({
	_tileSheet:null,
	_tileMap:null,
	_lastgen:0,
	_playerStart:0,
	ctor:function()
	{
		this._super();
		cc.spriteFrameCache.addSpriteFrames(res.Tiles_plist);
		this._tileSheet = new cc.SpriteBatchNode(res.Tiles_png);
		
		this._tileMap = {};
		
		this.addChild(this._tileSheet);
		//this.generateTiles(0, this.width / 64);
	},
	
	generateFloor:function()
	{
		this.loadRoom("room2", 0, 0, 0, 20);
		
		return this._playerStart;
	},
	
	loadRoom:function(roomname, xgen, ygen, croom, totalrooms)
	{
		var world = this;
		cc.loader.loadJson("res/placeholders/map/rooms/" + roomname + ".json", function(error, room) {
			var x = 0;
			var y = 0;
			var tile_type;
			for (var i in room.data)
			{
				tile_type = BackgroundTile.Back1;
				switch (room.data[i])
				{
				case 1:
					if (Math.random() > 0.8)
						tile_type = world.randomTile([BackgroundTile.Back2, BackgroundTile.Back3]);
					break;
				case 2:
					if (Math.random() > 0.8)
						tile_type = GroundTile.WallBack2;
					else
						tile_type = GroundTile.WallBack1;
					break;
				case 3:
					tile_type = GroundTile.WallL;
					break;
				case 4:
					tile_type = world.randomTile([GroundTile.Top1, GroundTile.Top2, GroundTile.Top3, GroundTile.Top4, GroundTile.TopGrass]);
					break;
				case 5:
					tile_type = GroundTile.WallR;
					break;
				case 6:
					tile_type = GroundTile.WallB;
					break;
				case 7:
					tile_type = GroundTile.WallTL;
					break;
				case 8:
					tile_type = GroundTile.WallTR;
					break;
				case 9:
					tile_type = GroundTile.WallBL;
					break;
				case 10:
					tile_type = GroundTile.WallBR;
					break;
				case 11:
					tile_type = ObstacleTile.LavaSolid;
					break;
				case 12:
					tile_type = world.randomTile([ObstacleTile.LavaTop1, ObstacleTile.LavaTop2]);
					break;
				}
				
				var tilex = x + room.startx + xgen;
				var tiley = y + room.starty + ygen;

				if (world.getTile(tilex, tiley) == undefined || !BackgroundTile.hasOwnValue(tile_type))
					world.makeTile(tile_type, tilex, tiley);

				
				switch(tile_type)
				{
				case GroundTile.TopGrass:
					if (BackgroundTile.hasOwnValue(world.getTile(tilex, tiley + 1).TileType))
						world.makeTile(DecorationTile.Grass1, tilex, tiley + 1);
					break;
				case GroundTile.WallB:
					if (Math.random() > 0.7)
						world.makeTile(DecorationTile.Stalactite1, tilex, tiley - 1);
					break;
				}
				
				x ++;
				if (x == room.width)
				{
					x = 0;
					y --;
				}
			}
			if (croom == 0)
				world._playerStart = cc.p(xgen, ygen);
			if (croom < totalrooms - 1)
				world.loadRoom("room2", room.endx + 1 + xgen, room.starty - room.endy + ygen, croom + 1, totalrooms);
		});
	},
	
	makeTile:function(tile_image, x, y)
	{
		var tile = new MapTile(tile_image, x, y);
		this._tileSheet.addChild(tile);

		if (!(x in this._tileMap))
			this._tileMap[x] = {};
		this._tileMap[x][y] = tile;
	},
	
	isSolidTile:function(x,y)
	{
		if (this.getTile(x,y) == undefined)
			return false;
		else
			return this.getTile(x,y).Solid;
	},

	getTile:function(x,y)
	{
		if (!(x in this._tileMap))
			return undefined;
		return this._tileMap[x][y];
	},

	randomTile:function(tilelist)
	{
		var t = Math.floor(tilelist.length * Math.random());
		return tilelist[t];
	},
	
	update:function(dt)
	{

	},

});

var MapTile = cc.Sprite.extend({
	TileType:null,
	Solid:null,
	ctor:function(type,x,y)
	{
		this._super(type);
		this.attr({ x:32 + x * 64, y:32 + y * 64});
		
		this.TileType = type;
		
		if (GroundTile.hasOwnValue(type))
			this.Solid = true;
		else
			this.Solid = false;
	}
});