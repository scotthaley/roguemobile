var GameScene = cc.Scene.extend({
	onEnter:function() {
		this._super();
		var layer = new GameLayer();
		this.addChild(layer);
	}
});

var GameLayer = cc.Layer.extend({
	_world:null,
	_cameraNode:null,
	_player:null,
	ctor:function()
	{
		this._super();
		
		this._cameraNode = new cc.Node();
		this._cameraNode.setScale(0.2);
		this.addChild(this._cameraNode);
		
		this._world = new World();
		this._cameraNode.addChild(this._world);
		
		this._player = new Player();
		this._cameraNode.addChild(this._player);
		
		var playerStart = this._world.generateFloor();
		this._player.setPosition(playerStart.x * 64 + 32, playerStart.y * 64 + 32);
		
		this.scheduleUpdate();
	},
	
	update:function(dt)
	{
		this._world.update(dt);
		var playerPos = this._player.getPosition();
		this._cameraNode.setPosition((-playerPos.x + 96) * 0.2, (-playerPos.y + 96 * 10) * 0.2)
		
		var gravity = -5;
		if (this._world.isSolidTile(Math.floor(playerPos.x / 64), Math.floor((playerPos.y - 32) / 64)))
			gravity = 0;
		
		this._player.setPosition(playerPos.x + 200 * dt, playerPos.y + gravity);
	}
});