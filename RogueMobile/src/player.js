var Player = cc.Node.extend({
	_sprite:null,
	_spriteSheet:null,
	_runAction:null,
	ctor:function()
	{
		this._super();

		cc.spriteFrameCache.addSpriteFrames(res.Player_plist);
		this._spriteSheet = new cc.SpriteBatchNode(res.Player_png);
		this.addChild(this._spriteSheet);
		
		var animFrames = [];
		//for (var i = 1; i < 4; i ++)
			//animFrames.push(cc.spriteFrameCache.getSpriteFrame("player" + i + ".png"));
		animFrames.push(cc.spriteFrameCache.getSpriteFrame("player1.png"));
		animFrames.push(cc.spriteFrameCache.getSpriteFrame("player2.png"));
		animFrames.push(cc.spriteFrameCache.getSpriteFrame("player3.png"));
		animFrames.push(cc.spriteFrameCache.getSpriteFrame("player2.png"));
		
		var animation = new cc.Animation(animFrames, 0.15);
		this._runAction = new cc.RepeatForever(new cc.Animate(animation));
		this._sprite = new cc.Sprite("#player1.png");
		this._sprite.runAction(this._runAction);
		this._spriteSheet.addChild(this._sprite);
	}
});